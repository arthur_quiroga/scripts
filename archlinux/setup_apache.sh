#!/bin/bash
#
# Web server installation and configuration.
# Apache, php, mariadb, phpmyadmin
#
# Distribution: Archlinux
# Author: Arthur Quiroga
# Date: 2017/02/27


########## Colors ##############################################################

BLACK='\033[0;30m'
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
LIGHTGRAY='\033[0;37m'
DARKGRAY='\033[1;30m'
LIGHTRED='\033[1;31m'
LIGHTGREEN='\033[1;32m'
YELLOW='\033[1;33m'
LIGHTBLUE='\033[1;34m'
LIGHTPURPLE='\033[1;35m'
LIGHTCYAN='\033[1;36m'
WHITE='\033[1;37m'
NC='\033[0m'

########## Functions ###########################################################

function install {
  echo -e "\n\n${YELLOW}-> Installation of $1 package${NC}"
  pacman -Qi $1 &> /dev/null
  if [[ $? -eq 1 ]]; then
    echo "y" | pacman -S $1
    echo -e "${GREEN}Package \"$1\" is now installed${NC}"
  else
    echo -e "${GREEN}Package \"$1\" is already installed${NC}"
  fi
}

function activeModule {
  bef="LoadModule $module $binary"
  sed -i.bak "s/$bef/#&/g" /etc/httpd/conf/httpd.conf
  echo -e "Module '$module' disabled"
}

########## Synchronization #####################################################

echo -e "${YELLOW}-> Synchronization of package databases${NC}"
pacman -Sy

########## Install #############################################################
install apache
install php
install php-apache
install mariadb
install phpmyadmin
install php-mcrypt

########## Php #################################################################

echo -e "${YELLOW}-> Php settings${NC}"

sed -i.bak 's/;extension=mysqli.so/extension=mysqli.so/g' /etc/php/php.ini
echo -e "Extension 'mysqli.so' enabled"
sed -i.bak 's/;extension=pdo_mysql.so/extension=pdo_mysql.so/g' /etc/php/php.ini
echo -e "Extension 'pdo_mysql.so' enabled"

########## Apache ##############################################################

echo -e "${YELLOW}-> Apache settings${NC}"

activeModule "mpm_event_module" "modules\/mod_mpm_event.so"
activeModule "mpm_prefork_module" "modules\/mod_mpm_prefork.so"

exist=`grep /etc/httpd/conf/httpd.conf -e "# Load php7 module"`

if [[ $? -eq 1 ]]; then
  bef="<IfModule unixd_module>"
  aft="# Load php7 module\nLoadModule php7_module modules\/libphp7.so\n\n"
  sed -i.bak "s/$bef/$aft&/g" /etc/httpd/conf/httpd.conf
fi

exist=`grep /etc/httpd/conf/httpd.conf -e "# PHP settings"`
if [[ $? -eq 1 ]]; then
  echo -e "# PHP settings\nInclude conf/extra/php7_module.conf" >> /etc/httpd/conf/httpd.conf
fi
echo -e "Module 'libphp7' enabled"

########## Mariadb #############################################################

echo -e "${YELLOW}-> Mariadb settings${NC}"

/usr/bin/mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql

########## PhpMyAdmin ##########################################################

echo -e "${YELLOW}-> PhpMyAdmin settings${NC}"

if [ ! -e /etc/httpd/conf/extra/httpd-phpmyadmin.conf ]; then
  text="Alias /phpmyadmin \"\/usr\/share\/webapps\/phpMyAdmin\"\n<Directory \"\/usr\/share\/webapps\/phpMyAdmin\">\nDirectoryIndex index.html index.php\nAllowOverride All\nOptions FollowSymlinks\nRequire all granted\n<\/Directory>"
  echo "$text" > /etc/httpd/conf/extra/httpd-phpmyadmin.conf
fi


exist=`grep /etc/httpd/conf/httpd.conf -e "# phpMyAdmin configuration"`
if [[ $? -eq 1 ]]; then
  echo -e "# phpMyAdmin configuration\nInclude conf\/extra\/httpd-phpmyadmin.conf" >> /etc/httpd/conf/httpd.conf
fi
